package main

import (
	"fmt"
)

func main() {
	largestPalidrome := 0
	for a := 999; a >= 100; a-- {
		for b := 999; b >= 100; b-- {
			c := a * b
			if c <= largestPalidrome {
				break
			}
			if isPalidrome(c) {
				largestPalidrome = c
			}

		}
	}
	fmt.Println(largestPalidrome)
}

func isPalidrome(input int) bool {

	reverse := 0
	number := input
	for number > 0 {
		lastDigit := number % 10
		reverse = (reverse * 10) + lastDigit
		number = number / 10
	}

	if input == reverse {
		return (true)
	}
	return (false)
}
