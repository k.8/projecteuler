package main

import "fmt"

func main() {
	nprime := 10001
	nth := 0
	i := 1

	for nth < nprime {
		i++
		if isPrime(i) == true {
			nth++
		}
	}
	fmt.Println(i)
}

func isPrime(i int) bool {
	for x := 2; x <= i; x++ {
		if x == i {
			return true
		}
		if i%x == 0 {
			return false
		}
	}
	return false
}
