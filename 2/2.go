package main

import "fmt"

func main() {
	var limit int = 4e18
	sumofeven := 2
	a := 1
	b := 2
	var c int
	for true {

		c = a + b
		if c > limit {
			break
		}
		if c%2 == 0 {
			sumofeven += c
		}
		a = b
		b = c

	}
	fmt.Println(sumofeven)
}
