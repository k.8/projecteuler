package main

import "fmt"

func main() {
	n := 600851475143
	lpf := 0
	for i := 2; i <= n; i++ {
		if n%i == 0 {
			n /= i
			lpf = i
			i--

		}

	}
	fmt.Println(lpf)
}
