package main

import "fmt"

func main() {
	n := 100
	fmt.Println(squareOfSum(n) - sumOfSquares(n))
}

func sumOfSquares(n int) int {
	sum := 0
	for i := 1; i <= n; i++ {
		sum += i * i
	}
	return sum
}

func squareOfSum(n int) int {
	square := 0
	for i := 1; i <= n; i++ {
		square += i
	}
	return (square * square)
}
