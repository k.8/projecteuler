package main

import "fmt"

func main() {
	n := 21
	for true {
		for i := 2; i <= 20; i++ {
			if n%i == 0 {
				if i == 20 {
					fmt.Println(n)
					return
				}
				continue
			} else {
				break
			}

		}
		n++
	}
}
